This repository contains the code for a full-stack exam-taking website developed by Umair Malik and Bakrtech. The website allows users to create, manage, and take exams online.
