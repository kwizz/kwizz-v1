const buttonContainer = document.getElementById("questionButtonWrapper")

let totalNoOfQuestions = 100

for (let i = 1 ; i <= totalNoOfQuestions ; i++) {
    const button = document.createElement("button");
    button.className = "questionNumbersInPanel";
    button.id = "qNo" + i;
    button.textContent = i.toString();
    buttonContainer.appendChild(button);
}